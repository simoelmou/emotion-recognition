from __future__ import print_function

import glob
import os.path
import pickle

import cv2
import numpy as np
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import BernoulliRBM
from sklearn.pipeline import Pipeline


# Function to load the train images and construct a target array
def load_images():
    joie = [glob.glob("./images/1-joie/*.png")]
    degout = [glob.glob("./images/2-degout/*.png")]
    tristesse = [glob.glob("./images/3-tristesse/*.png")]
    colere = [glob.glob("./images/4-colere/*.png")]
    surprise = [glob.glob("./images/5-surprise/*.png")]
    images = [cv2.imread(img, 0) for img in joie[0]]
    images.extend([cv2.imread(img, 0) for img in degout[0]])
    images.extend([cv2.imread(img, 0) for img in tristesse[0]])
    images.extend([cv2.imread(img, 0) for img in colere[0]])
    images.extend([cv2.imread(img, 0) for img in surprise[0]])
    target = []
    for i in range(len(joie[0])):
        target.append(0)
    for i in range(len(degout[0])):
        target.append(1)
    for i in range(len(tristesse[0])):
        target.append(2)
    for i in range(len(colere[0])):
        target.append(3)
    for i in range(len(surprise[0])):
        target.append(4)
    return images, target


# Prepare images, it converts the values to float and scale them
# so that they become between 0-1 so the classification can understand the values.
def prepare_data(images, target):
    n_samples = len(images)
    data = np.asarray(images, 'float32')
    data = data.reshape((n_samples, -1))
    Y_train = np.asarray(target)
    X_train = (data - np.min(data, 0)) / (np.max(data, 0) + 0.0001)  # 0-1 scaling
    return X_train, Y_train


print("==============================================================================")
print('                            Fixing Parameters')
print("==============================================================================")
hidden_layer_count = 600
parameters = {
    'rbm': BernoulliRBM(batch_size=10, learning_rate=0.005,
                        n_components=hidden_layer_count, n_iter=45,
                        random_state=1, verbose=True),
    'logistic': LogisticRegression(C=1, class_weight=None, dual=False,
                                   fit_intercept=True, intercept_scaling=1,
                                   penalty='l2', random_state=None, tol=0.0001)
}
results_filename = 'results.pickle'

print("==============================================================================")
print('                            Start')
print("==============================================================================")
# If the classifier was already trained and saved in a file, we load it using pickle library
if os.path.exists(results_filename):
    with open(results_filename, "rb") as pickle_file:
        rbm, logistic, classifier, metrics_results = pickle.load(pickle_file)
else:
    print("==============================================================================")
    print('                             Create classifier')
    print("==============================================================================")
    rbm = parameters["rbm"]
    logistic = parameters["logistic"]
    classifier = Pipeline(steps=[('rbm', rbm), ('logistic', logistic)])

    print("==============================================================================")
    print('                             Loading images')
    print("==============================================================================")
    images, target = load_images()

    print("==============================================================================")
    print('                             Data preparation')
    print("==============================================================================")
    X_train, Y_train = prepare_data(images, target)

    print("==============================================================================")
    print('                            Training')
    print("==============================================================================")
    # Training RBM-Logistic Pipeline
    classifier.fit(X_train, Y_train)

    print("==============================================================================")
    print('                            Evaluation')
    print("==============================================================================")
    metrics_results = metrics.classification_report(Y_train, classifier.predict(X_train))
    print()
    print("Test results of logistic regression using RBM features:\n{}\n".format(metrics_results))

    # saves the classifier and the evaluation results in a file
    with open(results_filename, "wb") as file:
        pickle.dump([rbm, logistic, classifier, metrics_results], file)

if __name__ == '__main__':
    print("==============================================================================")
    print('                            Done')
    print("==============================================================================")

# For better evaluation, the classifier's parameters should be optimized using CrossValidation,
# CrossValidation can be done using sklearn GridSearch.

# print("==============================================================================")
# print('                            CrossValidation')
# print("==============================================================================")
# # initialize the RBM + Logistic Regression pipeline
# rbm = BernoulliRBM(random_state=1, verbose=True)
# logistic = LogisticRegression()
# classifier = Pipeline(steps=[('rbm', rbm), ('logistic', logistic)])
#
# # perform a grid search on the learning rate, number of
# # iterations, and number of components on the RBM and
# # C for Logistic Regression
# print("SEARCHING RBM AND LOGISTIC REGRESSION'S HYPER-PARAMETERS")
# # The following params already have been edited manually
# # to then wiggle between
# # these remaining values.
# params = {
#     'rbm__learning_rate': [0.005, 0.008, 0.01, 0.06, 0.1],  # 0.005
#     'rbm__n_iter': [45, 20, 10],  # 45
#     'rbm__n_components': [100, 200, 400, 600],  # 600
#     'logistic__C': [0.001, 0.01, 0.1, 1, 10, 100, 1000]}  # 1
# # perform a grid search over the parameter
# gs = GridSearchCV(classifier, params, n_jobs=-1, verbose=1, cv=2)
# gs.fit(X_train, Y_train)
#
# # print diagnostic information to the user and grab the
# # best model
# print("best score: %0.3f" % (gs.best_score_))
# print("RBM + LOGISTIC REGRESSION'S HYPER-PARAMETERS")
#
# CV_result = gs.best_estimator_.get_params()
# print(CV_result)
