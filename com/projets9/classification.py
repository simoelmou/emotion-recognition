import numpy as np
import sys
from cv2 import cv2

from com.projets9.training import classifier


def predict_emotion(image, classifier):
    predicted_num = classifier.predict(image.reshape(1, -1))[0]
    decision_function_vals = classifier.decision_function(image.reshape(1, -1))[0]
    confidence = np.amax(decision_function_vals)
    # print("")
    # print("Predicted number: {}".format(predicted_num))
    # print("Confidence: {}".format(str(confidence)[:6]))
    return predicted_num, confidence


if __name__ == '__main__':
    path = (sys.argv[1])
    image = cv2.imread(path, 0)
    predicted_num, confidence = predict_emotion(image, classifier)

    print(predicted_num)
