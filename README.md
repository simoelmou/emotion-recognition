# Emotion Recognition

Emotion Recognition using Bernoulli RBM and Logistic Regression.

Data is represented with grayscale images of people with various emotions where pixel values can be interpreted as degrees of blackness on a white background.
